package models;

import play.data.format.Formats;
import play.data.validation.Constraints;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import io.ebean.Model;
import io.ebean.annotation.JsonIgnore;

import java.util.Date;

@Entity
@Table(name="person_tbl")
public class Person extends Model {

    @Id
    @Column(name="personId")
    public int personId;

    @Constraints.Required
    @ManyToOne()
    @JoinColumn(name="roleId")
    @JsonManagedReference
    public Role role;

    @Constraints.Required
    @Column(name="name")
    public String name;
    
    @Column(name="username")
    public String username;
    
    @Column(name="password")
    public String password;

    @Column(name="lastName")
    public String lastName;

    @Constraints.Required
    @Column(name="email")
    public String email;
    
    @Constraints.Required
    @Column(name="phone")
    public String phone;

    @Formats.DateTime(pattern="dd-MM-yyyy")
    @Column(name="createdAt")
    public Date createdAt;
    
    @Formats.DateTime(pattern="dd-MM-yyyy")
    @Column(name="updatedAt")
    public Date updatedAt;
    
    @Constraints.Required
    @Column(name="enabled")
    public Boolean enabled;
    
    public Person(Role role, String name, String email, Date createdAt, Date updatedAt, Boolean enabled) {
		//this.personId = personId;
		this.role = role;
		this.name = name;
		this.email = email;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.enabled = enabled;
	}
    
    public Person(Role role, String name, String lastName, String email, String phone, Date createdAt, Date updatedAt, Boolean enabled) {
		//this.personId = personId;
		this.role = role;
		this.name = name;
		this.lastName = lastName;
		this.email = email;
		this.phone = phone;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.enabled = enabled;
	}
    
}