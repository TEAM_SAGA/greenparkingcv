package models;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import io.ebean.Model;
import play.data.validation.Constraints;

@Entity
@Table(name="messages_tbl")
public class Messages extends Model{
	@Id
    @Column(name="messageId")
    public int messageId;
    
    @Constraints.Required
    @Column(name="messageTitle")
    public String messageTitle;
    
    @Constraints.Required
    @Column(name="messageContent")
    public String messageContent;
    
    public Messages(String messageTitle, String messageContent) {
    	this.messageTitle = messageTitle;
    	this.messageContent = messageContent;
    }
}
