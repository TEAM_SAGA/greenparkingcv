package models;

import play.data.validation.Constraints;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import io.ebean.Model;
import io.ebean.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.List;


@Entity
@Table(name="roles_tbl")
public class Role extends Model {

    @Id
    @Column(name="roleId")
    public int roleId;
    
    @Constraints.Required
    @Column(name="roleName")
    public String roleName;
    
    @OneToMany(mappedBy="role")
    @JsonBackReference
    public List<Person> persons;
    
    public Role(int roleId, String roleName) {
    	this.roleId = roleId;
    	this.roleName = roleName;
    }
    
}