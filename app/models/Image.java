package models;

import java.sql.Blob;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import io.ebean.Model;
import play.data.validation.Constraints.Required;


@Entity
@Table(name="images_tbl")
public class Image extends Model {

    @Id
    @Column(name="imageId")
    public int imageId;	 // pk

    @Column(name="url")
    @Required
    public String url;
    
    @Column(name="description")
    public String description;
    
    public Image(String url, String description) {
    	this.url = url;
    	this.description = description;
    }
    
}