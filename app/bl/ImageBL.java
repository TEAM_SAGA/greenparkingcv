package bl;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import dal.ImageDAL;
import models.Image;
import exceptions.SpecialCharsException;

public class ImageBL {

	public List<Image> getAllImages() {
		ImageDAL dal = new ImageDAL();
		
		return dal.getAllImages();
	}
	
	public List<Image> getImageByDesc(String desc) throws SpecialCharsException {
		ImageDAL dal = new ImageDAL();
		Pattern pattern = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(desc);

		if(matcher.find()) {
			throw new SpecialCharsException("bad image description");
		}
		
		return dal.getImageByDesc(desc);
	}
	
}
