package bl;

import java.util.List;

import dal.messagesDAL;
import models.Messages;

public class MessagesBL {
	
	public Boolean deleteMessage(String messageTitle) {
		messagesDAL message_dal = new messagesDAL();
		
		return message_dal.deleteMessage(messageTitle);
	}
	
	public Boolean createMessage(String messageTitle, String messageContent) {
		messagesDAL message_dal = new messagesDAL();
		Messages message = message_dal.getMessageByTitle(messageTitle);
		
		if(message != null) {
			return false;
		}
		
		return message_dal.createMessage(messageTitle, messageContent);
	}
	
	public List<Messages> getAllMessages() {
		messagesDAL message_dal = new messagesDAL();
		
		return message_dal.getAllMessages();
	}
}
