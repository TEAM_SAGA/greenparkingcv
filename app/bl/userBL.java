package bl;
import java.util.List;

import dal.roleDAL;
import dal.userDAL;
import models.Person;
import models.Role;

public class userBL {
	
	final static String ADMIN = "admin";
	public boolean login(String username, String password) {
		System.out.println("USED ATTEMPTED LOGIN:"+username+password);
		userDAL user_dal = new userDAL();
		Person person= user_dal.personByUsername(username, password);	
		System.out.println("and his role is:"+person.role.roleId);
		if(person != null ) {
			// if(person.role.roleName.equals(ADMIN))
			
			if(person.role.roleId==1)
				return true;
		}	
		return false;
	}
	
	public List<Person> personsByRole(String roleName) {
		userDAL dal = new userDAL();
		
		return dal.personsByRole(roleName);
	}
	
	public boolean createPerson(int personId, 
								String roleName, 
								String name, 
								String lastName, 
								String email,
								String phone,
								Boolean enabled) {
		userDAL user_dal = new userDAL();
		roleDAL role_dal = new roleDAL();
		Role role = role_dal.getRoleByName(roleName);
		Person person = user_dal.personById(personId);
		if (role == null || person != null) {
			return false;
		}
		
		return user_dal.createPerson(role, name, lastName, email, phone, enabled);
	}
	
	public boolean updatePerson(int personId, String roleName, String name, String lastName, String email, Boolean enabled) {
		userDAL user_dal = new userDAL();
		roleDAL role_dal = new roleDAL();
		Role role = role_dal.getRoleByName(roleName);
		Person person = user_dal.personById(personId);
		
		if (role == null || person == null) {
			return false;
		}
		
		return user_dal.updatePerson(person, role, name, lastName, email, enabled);
	}
	
	public boolean deletePerson(int personId) {
		userDAL user_dal = new userDAL();
		
		return user_dal.deletePerson(personId);
	}
	
}
