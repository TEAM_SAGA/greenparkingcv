package services;

import play.libs.mailer.Email;
import play.libs.mailer.MailerClient;
import java.util.ArrayList;
import org.apache.commons.mail.EmailException;

public class MailerService {
	  MailerClient mailerClient;

	  public MailerService(MailerClient mailerClient){
	    this.mailerClient = mailerClient;
	  }

	  public void sendEmail(String mailTitle, 
			  				String mailContent, 
			  				ArrayList<String> customers) throws EmailException{
	    Email email = new Email();
	    email.setSubject(mailTitle);
	    email.setFrom("playSagaTest@gmail.com");
	    email.setTo(customers);
	    email.setBodyText(mailContent);
	    mailerClient.send(email);
	  }
}
