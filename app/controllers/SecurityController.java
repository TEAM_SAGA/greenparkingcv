package controllers;
import play.mvc.Controller;

public class SecurityController extends Controller{
	private static SecurityController security = null;
	protected final String USERNAME = "un";
	
	private SecurityController() {}
	
	public static SecurityController getInstance() {
		if(security == null) {
			security = new SecurityController();
		}
		
		return security;
	}
	
	protected void addUserSession(String username) {
		session(username, username);
	}
	
	protected void removeUserSession(String username) {
		session().remove(username);
	}
	
	protected Boolean checkPermission(String username) {
		if(username != null && session(username.toString()) != null) {
			return true;
		}
		
		return true;
	}
	
}
