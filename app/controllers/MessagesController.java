package controllers;

import java.util.Map;
import java.util.TreeMap;
import com.fasterxml.jackson.databind.JsonNode;
import bl.MessagesBL;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

public class MessagesController extends Controller{
	
	public Result getAllMessages() {
		if(SecurityController.
				   getInstance().
				   checkPermission(
						   request().
						   getQueryString(SecurityController.getInstance().USERNAME))) {
			MessagesBL bl = new MessagesBL();
			JsonNode messagesJson = Json.toJson(bl.getAllMessages());
		
			return ok(messagesJson);
		}
		
		return unauthorized("bad request");
    }
	
	public Result createMessage() {
		if(SecurityController.
				   getInstance().
				   checkPermission(
						   request().
						   getQueryString(SecurityController.getInstance().USERNAME))) {
			MessagesBL bl = new MessagesBL();
			JsonNode json = request().body().asJson();
			String messagesTitle = json.get("messageTitle").asText();
			String messagesContent = json.get("messageContent").asText();
	        
			if(bl.createMessage(messagesTitle, messagesContent)) {
				return ok("added");
			}
	        
			return ok("internal error");
		}
		
		return unauthorized("bad request");
    }
	
	public Result deleteMessage() {
		MessagesBL bl = new MessagesBL();
		String title = "";
		final int PARAM = 0; 
		TreeMap<String, String[]> messagesJson1 = new TreeMap<String, String[]>();
		messagesJson1.putAll(request().queryString());


		for (Map.Entry<String, String[]> entry : messagesJson1.entrySet())
		{
		    title += entry.getValue()[PARAM];
		    title += " ";
		}
		title = title.trim();
	
		if(bl.deleteMessage(title)) {
			return ok("deleted");
		}
        
		return ok("no such message");
    }
}
