package controllers;

import play.libs.Json;
import play.libs.mailer.MailerClient;
import play.mvc.*;
import java.util.ArrayList;
import org.apache.commons.mail.EmailException;
import controllers.SecurityController;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.inject.Inject;
import bl.userBL;
import services.MailerService;

public class userController extends Controller{
	@Inject MailerClient mClient;
	
	public Result login() {
        userBL bl = new userBL();
		JsonNode json = request().body().asJson(); 
		String username = json.get("username").asText();
		String password = json.get("password").asText();
        
        if(bl.login(username, password)) {
        	SecurityController.getInstance().addUserSession(username);
        	return ok("Success");
        }
        
		return badRequest("wrong username or password");
    }
	
	public Result sendEmail() {
		MailerService service = new MailerService(mClient);
		JsonNode json = request().body().asJson();
		
		try {
			ArrayList<String> customers = new ArrayList<String>();
			String mailTitle = json.get("mailTitle").asText();
			String mailContent = json.get("mailContent").asText();
			
			for (JsonNode customer : json.withArray("customers")) {
			   customers.add(customer.get("email").asText());
			}
			
			service.sendEmail(mailTitle, mailContent, customers);
		}catch (NullPointerException e) {
			return badRequest();
		}catch (EmailException e) {
			return internalServerError(e.getMessage());
		}
		
		return ok("messages sent succesfully");
    }
	
	public Result getCustomers() {
		if(SecurityController.
		   getInstance().
		   checkPermission(
				   request().
				   getQueryString(SecurityController.getInstance().USERNAME))) {
			String role = "customer";
			userBL bl = new userBL();
			JsonNode personsJson = Json.toJson(bl.personsByRole(role));
	        
			return ok(personsJson);
		}
		
		return unauthorized("bad request");
    }
	
	public Result createPerson() {
		if(SecurityController.
				   getInstance().
				   checkPermission(
						   request().
						   getQueryString(SecurityController.getInstance().USERNAME))) {
			userBL bl = new userBL();
			JsonNode json = request().body().asJson(); 
			int personId = json.get("personId").asInt();
			String roleName = json.get("roleName").asText();
			String name = json.get("name").asText();
			String lastName = json.get("lastName").asText();
			String email = json.get("email").asText();
			String phone = json.get("phone").asText();
			Boolean enabled = json.get("enabled").asBoolean();
			java.lang.System.out.println(json);
			if(bl.createPerson(personId,roleName, name, lastName, email, phone, enabled)) {
				return ok("added");
			}
	        
			return ok("internal error");
		}
		
		return unauthorized("bad request");
    }
	
	public Result updatePerson() {
		if(SecurityController.
				   getInstance().
				   checkPermission(
						   request().
						   getQueryString(SecurityController.getInstance().USERNAME))) {
			userBL bl = new userBL();
			JsonNode json = request().body().asJson();
			int personId = json.get("personId").asInt();
			String roleName = json.get("roleName").asText();
			String name = json.get("name").asText();
			String lastName = json.get("lastName").asText();
			String email = json.get("email").asText();
			Boolean enabled = json.get("enabled").asBoolean();
			if(bl.updatePerson(personId, roleName, name, lastName, email, enabled)) {
				return ok("updated");
			}
	        
			return ok("internal error");
		}
		
		return unauthorized("bad request");
    }
	
	public Result deletePerson(int personId) {
		if(SecurityController.
				   getInstance().
				   checkPermission(
						   request().
						   getQueryString(SecurityController.getInstance().USERNAME))) {
			userBL bl = new userBL();
			int pId = personId;
		
			if(pId != -1 && bl.deletePerson(pId)) {
				return ok("deleted");
			}
	        
			return ok("no such user");
		}
		
		return unauthorized("bad request");
    }
}
