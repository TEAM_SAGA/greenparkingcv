package controllers;

import play.libs.Json;
import play.libs.mailer.MailerClient;
import play.mvc.*;
import java.util.ArrayList;
import org.apache.commons.mail.EmailException;
import controllers.SecurityController;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.inject.Inject;
import bl.roleBL;
import services.MailerService;

public class roleController extends Controller{
	@Inject MailerClient mClient;
	
	public Result getAll() {
		if(SecurityController.
		   getInstance().
		   checkPermission(
				   request().
				   getQueryString(SecurityController.getInstance().USERNAME))) {
			roleBL bl = new roleBL();
			JsonNode roleJson = Json.toJson(bl.getAll());
	        
			return ok(roleJson);
		}
		
		return unauthorized("bad request");
    }
}
