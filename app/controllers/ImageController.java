package controllers;

import play.mvc.Controller;
import play.mvc.Result;
import play.libs.Json;

import java.util.List;

import bl.ImageBL;
import models.Image;

public class ImageController extends Controller{
	
	public Result getAllImages() {
        ImageBL bl = new ImageBL();
        List<Image> images = bl.getAllImages();
        
		return ok(Json.toJson(images));
    }
	
	public Result getImageByDesc(String desc) {
        ImageBL bl = new ImageBL();
        List<Image> images;
        
        try {
			images = bl.getImageByDesc(desc);
		} catch (Exception e) {
			return badRequest(e.getMessage());
		}
        
		return ok(Json.toJson(images));
    }
}
