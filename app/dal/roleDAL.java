package dal;

import java.util.List;
import java.util.ArrayList;

import io.ebean.Ebean;
import io.ebean.Query;
import models.Role;

public class roleDAL {

	public Role getRoleById(int id) {
		Role role = Ebean.createQuery(Role.class).where().eq("roleId", id).findOne();
		
		return role;
	}
	
	public Role getRoleByName(String roleName) {
		Role role = Ebean.createQuery(Role.class).where().eq("roleName", roleName).findOne();
		
		return role;
	}
	
	public List<Role> getAll() {
		List<Role> roles = Ebean.createQuery(Role.class).findList();
		
		return roles;
	}
	
	public void createRole(int id, String roleName) {
		Role role = new Role(id, roleName);
		role.save();
	}
	
}
