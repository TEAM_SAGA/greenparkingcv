package dal;

import java.util.List;

import javax.persistence.OptimisticLockException;

import io.ebean.Ebean;
import models.Messages;;

public class messagesDAL {

	public Messages getMessageByTitle(String title) {
		Messages message = Ebean.createQuery(Messages.class).where()
														 .eq("messageTitle", title)
														 .findOne();
		
		return message;
	}
	
	public List<Messages> getAllMessages() {
		List<Messages> messages = Ebean.createQuery(Messages.class).findList();
		
		return messages;
	}
	
	public Boolean createMessage(String messageTitle, String messageContent) {
		try {
			Messages message = new Messages(messageTitle, messageContent);
			message.save();
			
			return true;
		}catch (OptimisticLockException e) {
			return false;
		}
	}
	
	public Boolean deleteMessage(String messageTitle) {
		Messages message = Ebean.createQuery(Messages.class).where()
															.eq("messageTitle", messageTitle)
															.findOne();
		if(message == null) {
			return false;
		}
		
		message.delete();
		
		return true;
	}

	
}
