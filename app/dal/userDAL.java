package dal;

import java.util.Date;
import java.util.List;

import javax.persistence.OptimisticLockException;

import io.ebean.Ebean;
import models.Person;
import models.Role;

public class userDAL {

	public boolean login(String username, String password) {
		return false;
	}
	
	public List<Person> personsByRole(String roleName) {
		List<Person> persons = Ebean.createQuery(Person.class)
								   .where()
								   .eq("role.roleName", roleName)
								   .findList();
		
		return persons;
	}
	
	public boolean createPerson(Role role, 
								String name, 
								String lastName, 
								String email,
								String phone,
								Boolean enabled) {
		try {
			Date createdAt = new Date(System.currentTimeMillis());
			Date updatedAt = new Date(System.currentTimeMillis());
			Person person = new Person(role, name, lastName, email, phone, createdAt, updatedAt, enabled);
			person.save();
			return true;
		}catch (OptimisticLockException e) {
			return false;
		}
	}
	
	public Person personById(int personId) {
		Person person = Ebean.createQuery(Person.class)
	             			 .where()
	             			 .eq("personId", personId)
	             			 .findOne();
		
		if(person == null) {
			return null;
		}
		
		return person;
	}
	
	public Person personByUsername(String username, String password) {
		Person person = Ebean.createQuery(Person.class)
	             			 .where().and()
	             			 .eq("username", username)
	             			 .eq("password", password)
	             			 .findOne();
		
		if(person == null) {
			return null;
		}
		
		return person;
	}
	
	public boolean updatePerson(Person person, 
								Role role,  
								String name, 
								String lastName, 
								String email, 
								Boolean enabled) {
		try {
			Date updatedAt = new Date(System.currentTimeMillis());
			person.role = role;
			person.name = name;
			person.lastName = lastName;
			person.email = email;
			person.updatedAt = updatedAt;
			person.enabled = enabled;
			person.update();
			return true;
		}catch (OptimisticLockException e) {
			return false;
		}
	}
	
	public boolean deletePerson(int personId) {
		Person person = Ebean.createQuery(Person.class).where().eq("personId", personId).findOne();
		
		if(person == null) {
			return false;
		}
		
		person.delete();
		
		return true;
	}
	
	
}
