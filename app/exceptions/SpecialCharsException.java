package exceptions;

public class SpecialCharsException extends Exception{

    public SpecialCharsException(String message) {
        super(message);
    }
}
