import React from 'react';


import ContactUsAdvert from '../components/Contact_us_components/Contact_us_advert';
import ContactForm from '../components/Contact_us_components/Contact_form';
export default class ContactUs extends React.Component{
    render() {
        return (
            <div className="container">
                <ContactForm></ContactForm>
                <ContactUsAdvert></ContactUsAdvert>
            </div>
        );
    }
}
