import React from "react";
import MainDescription from '../components/main_description';
import SideDescription from '../components/side_description';
import { Row, Grid, Col } from 'react-bootstrap'
import GalleryRotate2 from '../components/Gallery_components/GalleryRotate2';

export default class Home extends React.Component{
    render() {
        return (
            <Grid>
                <Row>
                    <Col xs={12} className="main_description">
                        <GalleryRotate2/>>
                    </Col>
                </Row>
            </Grid>
        );
    }
}