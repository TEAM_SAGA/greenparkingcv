import React from "react";
import ImageGallery from "react-image-gallery";
import axios from 'axios';
import 'primereact/resources/themes/omega/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
const PREFIX_URL = 'https://raw.githubusercontent.com/xiaolin/react-image-gallery/master/static/';

export default class GalleryRotate extends React.Component {

    constructor() {
        super();
        this.state = {
            imeg:[],
            showIndex: false,
            showBullets: false,
            infinite: true,
            showThumbnails: true,
            showFullscreenButton: false,
            showGalleryFullscreenButton: false,
            showPlayButton: false,
            showGalleryPlayButton: false,
            showNav: true,
            slideDuration: 450,
            slideInterval: 2000,
            thumbnailPosition: 'bottom',
            showVideo: {},
        };

        this.images = [];
        this._myImageGalleryRenderer=this._myImageGalleryRenderer.bind(this);
    }
    async componentDidMount(){
        await this._getStaticImages();
    }
    componentDidUpdate(prevProps, prevState) {
        if (this.state.slideInterval !== prevState.slideInterval ||
            this.state.slideDuration !== prevState.slideDuration) {
            // refresh setInterval
            this._imageGallery.pause();
            this._imageGallery.play();
        }
    }

    _onImageClick(event) {
        console.debug('clicked on image', event.target, 'at index', this._imageGallery.getCurrentIndex());
    }

    _onImageLoad(event) {
        console.debug('loaded image', event.target.src);
    }

    _onSlide(index) {
        this._resetVideo();
        console.debug('slid to index', index);
    }

    _onPause(index) {
        console.debug('paused on index', index);
    }

    _onScreenChange(fullScreenElement) {
        console.debug('isFullScreen?', !!fullScreenElement);
    }

    _onPlay(index) {
        console.debug('playing from index', index);
    }

    _handleInputChange(state, event) {
        this.setState({[state]: event.target.value});
    }

    _handleCheckboxChange(state, event) {
        this.setState({[state]: event.target.checked});
    }

    _handleThumbnailPositionChange(event) {
        this.setState({thumbnailPosition: event.target.value});
    }

    async _getStaticImages() {
        let images = [];
        let url = 'http://localhost:9090/image';
        await axios.get(url).then(res=>{
            console.log("ze images are");
            console.log(res.data);
                for (let i = 0; i < res.data.length; i++) {
                    images.push({
                        original: ("http://localhost:9090/staticimage/"+res.data[i].url),
                        thumbnail:("http://localhost:9090/staticimage/"+res.data[i].url),
                        description:res.data[i].description,
                    });
                }
            });
        console.log(images);
        this.images=images;
        this.setState({imeg:images});
    }

    _resetVideo() {
        this.setState({showVideo: {}});

        if (this.state.showPlayButton) {
            this.setState({showGalleryPlayButton: true});
        }

        if (this.state.showFullscreenButton) {
            this.setState({showGalleryFullscreenButton: true});
        }
    }

    _toggleShowVideo(url) {
        // this.state.showVideo[url] = !Boolean(this.state.showVideo[url]);
        this.setState({
            showVideo: this.state.showVideo
        });

        if (this.state.showVideo[url]) {
            if (this.state.showPlayButton) {
                this.setState({showGalleryPlayButton: false});
            }

            if (this.state.showFullscreenButton) {
                this.setState({showGalleryFullscreenButton: false});
            }
        }
    }

    _renderVideo(item) {
        return (
            <div className='image-gallery-image' style={{backgroundColor:"lime"}}>
                {
                    this.state.showVideo[item.embedUrl] ?
                        <div className='video-wrapper'>
                            <a
                                className='close-video'
                                onClick={this._toggleShowVideo.bind(this, item.embedUrl)}
                            >
                            </a>
                            <iframe title="title"
                                width='560'
                                height='315'
                                src={item.embedUrl}
                                frameBorder='0'
                                allowFullScreen
                            >
                            </iframe>
                        </div>
                        :
                        <a onClick={this._toggleShowVideo.bind(this, item.embedUrl)}>
                            <div className='play-button'></div>
                            <img alt="" src={item.original}/>
                            {
                                item.description &&
                                <span
                                    className='image-gallery-description'
                                    style={{right: '0', left: 'initial'}}
                                >
                    {item.description}
                  </span>
                            }
                        </a>
                }
            </div>
        );
    }
    _myImageGalleryRenderer(item) {
        // your own image error handling
        const onImageError = this._handleImageError
        // console.clear();
        console.log(item.description);
        var lines = item.description.split(/\r\n|\r|\n/g);
        console.log("len is:"+lines.length);
        const listItems = lines.map((line) =>
            <p>{line}</p>
            );
        return (
            <div className="ui-g">
                <div className="ui-g-12 ui-md-5">
                    <div className='image-gallery-image'>
                        <img
                            src={item.original}
                            alt={item.originalAlt}
                            srcSet={item.srcSet}
                            sizes={item.sizes}
                            onLoad={this.props.onImageLoad}
                            
                            />
                
                        {/* {
                            item.description &&
                            <span className='image-gallery-description'>
                            {item.description}
                            {
                                item.link && item.linkLabel &&
                                <a className='my-own-class' href={item.link}>
                                    {item.linkLabel}
                                </a>
                            }
                            </span>
                        } */}
                    </div>
                </div>
                <div className="ui-g-12 ui-md-7" style={{textAlign:"right",marginTop:'20px',lineHeight:'1em',whiteSpace:"pre-line"}}>
                        {`${item.description}`};
                </div>
            </div>
        )
      }
 
    render() {
        return (

            <section className='app' style={{backgroundColor:'#2B5200'}}>
                <ImageGallery
                    ref={i => this._imageGallery = i}
                    items={this.state.imeg}
                    lazyLoad={false}
                    onClick={this._onImageClick.bind(this)}
                    onImageLoad={this._onImageLoad}
                    onSlide={this._onSlide.bind(this)}
                    onPause={this._onPause.bind(this)}
                    onScreenChange={this._onScreenChange.bind(this)}
                    onPlay={this._onPlay.bind(this)}
                    infinite={this.state.infinite}
                    showBullets={this.state.showBullets}
                    showFullscreenButton={this.state.showFullscreenButton && this.state.showGalleryFullscreenButton}
                    showPlayButton={this.state.showPlayButton && this.state.showGalleryPlayButton}
                    showThumbnails={this.state.showThumbnails}
                    showIndex={this.state.showIndex}
                    showNav={this.state.showNav}
                    thumbnailPosition={this.state.thumbnailPosition}
                    slideDuration={Number(this.state.slideDuration)}
                    slideInterval={Number(this.state.slideInterval)}
                    renderItem={this._myImageGalleryRenderer}
                    additionalClass="app-image-gallery"
                />
            </section>
        );
    }
}