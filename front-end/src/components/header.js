import React, { Component } from 'react';
import { Row, Col, Modal,Image,Button,ButtonToolbar } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import {InputText} from 'primereact/inputtext';
import {Messages} from 'primereact/messages';
import {Message} from 'primereact/message';
import axios from 'axios';
import { CookiesProvider } from 'react-cookie';
import cookie from 'react-cookies'
export default class Header extends React.Component{
    constructor(){
        super();
        this.state={username:"",
                    password:"",
                    show: false,
                    userId:"",
                };
        this.login=this.login.bind(this);
        this.handleShow = this.handleShow.bind(this);
        this.handleClose = this.handleClose.bind(this);
    
      }
    
    handleClose() {
        this.setState({ show: false });
    }
    
    handleShow() {
        this.setState({ show: true });
    }
    async login(){
        let url = 'http://localhost:9090/person/login';
        await axios.post(url, {
            username: this.state.username,
            password: this.state.password,
          })
          .then(async (response)=> {
            console.log(response);
            // const expires = new Date()
            // expires.setDate(Date.now() + 1000);
            // await cookie.save('userId', this.state.username);
            // console.log(cookie.load('userId'));
            // this.setState(this.state);
            this.props.onlogin(this.state.username);
            this.handleClose();
        })
          .catch(function (error) {
            console.log(error);
          });
    }
    islogged(){
        if(cookie.load('userId')){
            return <Col xs={3} className="private_area_loggedin">
                        <h1 className="private_area_text" >{cookie.load('userId')+" ברוך הבא"}</h1>
                        <p  style={{textAlign:'right'}}>ליצאה, לחץ<a style={{cursor:'pointer'}} onClick={this.props.logout}> כאן</a> </p>
                    </Col>
        }
        else
         return <Col xs={3} className="private_area" onClick={this.handleShow} style={{textAlign:'right'}} >
        <img src="http://localhost:9090/staticimage/private_area.png"/>
        <h1 className="private_area_text">כניסה לעמוד אישי</h1>
    </Col>
    }
    render() {
        return (
            <div className="container header">
                <Row>
                    <Modal show={this.state.show} onHide={this.handleClose} container={this} aria-labelledby="contained-modal-title"  bsSize="small">
                        <Modal.Header  >
                            <Modal.Title id="contained-modal-title" style={{textAlign:'right'}}>
                                כניסה
                            </Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <Row  >
                                <form>
                                    <Col className="heb-grid" xs={12}>
                                        <InputText placeholder={"שם משתמש"}  onChange={(e) => this.setState({username: e.target.value}) } style={{textAlign:'right'}}></InputText>
                                    </Col>
                                    <Col xs={12 } className="heb-grid" >
                                            <InputText style={{textAlign:'right'}} placeholder={"סיסמה"}   onChange={(e) => this.setState({password: e.target.value})}></InputText>
                                    </Col>
                                </form> 
                            </Row>
                        </Modal.Body>
                            <Modal.Footer>
                                <Row  >
                                    <Col xs={12}  >
                                        <ButtonToolbar style={{display:'flex',justifyContent:'space-between'}}>
                                            <Button bsStyle="info" style={{margin:0}}>הרשם</Button>
                                            <Button bsStyle="warning" style={{margin:0}} onClick={(e)=>{this.login();}}>התחבר</Button>
                                        </ButtonToolbar>
                                    </Col>
                                </Row>
                            </Modal.Footer>         
                        </Modal>
                        {this.islogged()}
                    <Col xsOffset={6} xs={3} className="logo">
                        <h1><Link to="/">חניון ירוק</Link></h1>
                    </Col>
                </Row>
            </div>
        );
    }
}
