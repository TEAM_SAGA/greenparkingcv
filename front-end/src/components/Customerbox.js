import React from "react";
import axios from 'axios';
import { Modal,Col,FormGroup, Button } from 'react-bootstrap';
import Customerresultbox from '../components/Customerbox/Customerresultbox3';
import {Growl} from 'primereact/growl';
import {SplitButton} from 'primereact/splitbutton';
import 'primereact/resources/themes/omega/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import {Button as PButton} from 'primereact/button';
import {Menubar} from 'primereact/menubar';
import {InputText} from "primereact/inputtext";
import AddCustomer from '../components/Customerbox/AddCustomer';
import ReactQuill from 'react-quill'; // ES6
import 'react-quill/dist/quill.snow.css'; // ES6
import { browserHistory } from 'react-router'
import {ContextMenu} from 'primereact/contextmenu';
export default class Costumerbox extends React.Component{
    constructor(){
        super();
        this.handleHide = this.handleHide.bind(this);
        this.quillRef = null;      // Quill instance
        this.reactQuillRef = null; // ReactQuill component
        this.state={
            customers:[],
            searchword:"",
            selectedcustomers:[],
            show: false,
            text:"",
            Senditems: [
                {
                    label: 'SMS', 
                    icon: 'fa fa-refresh', 
                    command: (e) => {
                        let message = this.handletext(this.state.text,true);
                        this.growl.show({severity:'success', summary:'Sent', detail:message});
                        this.Sendsms(message);
                        console.log("preez?");
                        axios.get('http://localhost:9090/api/person')
                            .then(function (response) {
                                // handle success
                                console.log(response);
                            })
                            .catch(function (error) {
                                // handle error
                                console.log(error);
                            });
                    }
                },
                {
                    label: 'EMAIL', 
                    icon: 'fa fa-close',
                    command: (e) => {
                        let message = this.handletext(this.state.text,false);
                        this.Sendemail(message);
                        this.growl.show({ severity: 'success', summary: 'Sent', detail:message });
                    
                    }
                },

            ],
            items: [{
                label: 'File',
                icon: 'fa fa-fw fa-file-o',
                items: [{
                    label: 'Save custom message',
                    icon: 'pi pi-save',
                    // onContextMenu:(e) =>{console.log("asdasdd");},
                    onMouseOver:(e)=>{console.log('glglgl')},
                    command: async(e) => {
                        // this.Sendsms();]
                        let title = prompt("Enter title name");
                        this.growl.show({severity:'success', summary:'Saved', detail:"Added "+title+" to database"});
                        let url = 'http://localhost:9090/message';
                        let message = this.handletext(this.state.text,false);
                        await axios.post(url, {
                            messageTitle: title,
                            messageContent: message
                          })
                          .then(function (response) {
                            console.log(response);
                          })
                          .catch(function (error) {
                            console.log(error);
                          });
                          this.updateloadmessage();
                    }
                },
                {
                    label: 'Load ',
                    items:"",
                    icon: 'pi pi-upload',
                },
                {
                    label: 'Delete ',
                    items:"",
                    icon: 'pi pi-trash',
                },
                ]
            }],

            
        };
        this.getsaveditems.bind(this);
        this.save = this.save.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handletext= this.handletext.bind(this);
        this.updateloadmessage=this.updateloadmessage.bind(this);
        this.getcustomers=this.getcustomers.bind(this);
        this.Sendemail=this.Sendemail.bind(this);
    }
    componentDidMount() {
        this.attachQuillRefs();
        this.updateloadmessage();
        this.getcustomers();
      }
    updateloadmessage(){
        this.getsaveditems().then((e)=>{
            let newarray = JSON.parse(JSON.stringify(e));
            let olditems=this.state.items;
            console.log("e is:");
            console.log(e);
            console.log(olditems[0].items[1]);
            for(let i=0;i<e.length;i++){
                newarray[i].command= (e) => {
                    this.growl.show({ severity: 'success', summary: 'Sent', detail:"test" });
                    this.setState((prevState, props) => ({
                        text: e.item.value
                      }));
                }
            }
            olditems[0].items[1].items=newarray;
            for(let i=0;i<e.length;i++){
                e[i].command= async (e) => {
                    var title=e.item.label;
                    var res = title.split(" ");
                    console.log("res.length"+res.length); 
                    var newtitle="";
                    for(let i =0;i<res.length;i++){
                        newtitle+='key'+i.toString()+'='+res[i]+'&';
                    }
                    newtitle=newtitle.substring(0,newtitle.length-1);
                    let url = 'http://localhost:9090/message?'+newtitle;//bob da boy -> data=bob&data=da&data=boy data=bob da boy
                    console.log("da url is:"+url);
                    console.log(e.item);
                    await axios.delete(url).then(res => {
                        this.updateloadmessage();
                        console.log(res);
                        console.log(res.data);
                      });
                    this.growl.show({ severity: 'success', summary: 'Sent', detail:"Deleted" });
                }
            }
            olditems[0].items[2].items=e;
            this.setState({items:olditems});
            
        });
    }
    async getcustomers(){
        
        await axios.get('http://localhost:9090/person')
        .then( (response)=> {
            // handle success
            console.log("the3 customers are:");
            console.log(response);
            this.setState({customers:response.data});
        })
        .catch(function (error) {
            // handle error
            console.log(error);
        }); 
        console.log(this.state);
    } 
    componentDidUpdate() {
      this.attachQuillRefs()
    }
      
      attachQuillRefs = () => {
        if (typeof this.reactQuillRef.getEditor !== 'function') return;
        this.quillRef = this.reactQuillRef.getEditor();
      }
    Sendemail(e){
        // console.log("send email not implemented yet");
        //makin array with emails
        let emails=[];
        for (let index = 0; index < this.state.selectedcustomers.length; index++) {
            emails.push({email:this.state.selectedcustomers[index].email});
            
        }
        console.log("emails:");
        console.log(emails);
        let url = 'http://localhost:9090/person/email';
        axios.put(url, {
            mailTitle: "titletest",
            mailContent: this.handletext(this.state.text,true),
            customers:emails,
          })
          .then(function (response) {
            console.log(response);
          })
          .catch(function (error) {
            console.log(error);
          });
          this.updateloadmessage();
    }
    Sendsms(e){
        console.log("send sms not implemented yet");
    }
    handletext(e,forsms){
        const editor = this.reactQuillRef.getEditor();
        const unprivilegedEditor = this.reactQuillRef.makeUnprivilegedEditor(editor);
        if(e!=null&&e!==""&&this.quillRef!=null){
            return (forsms)?unprivilegedEditor.getText():unprivilegedEditor.getHTML();
        }
        return ""
        
    }
    handleChange(value) {
        this.setState({ text: value })
        console.log(value);
      }
    handleHide() {
        this.setState({ show: false });
        this.getcustomers();
      }
    handlesearchchange(e){
        console.log(e.target.value);
    }
    handleClick(e){ 
        let selectedstring="";
        for(let i = 0;i<this.state.selectedcustomers.length;i++){
            console.log("i is:"+i+this.state.selectedcustomers[i].name+" "+this.state.selectedcustomers[i].id);
            selectedstring+=this.state.selectedcustomers[i].name+" "+this.state.selectedcustomers[i].id+"\n";
        }
        alert(selectedstring);
        // alert("The selected customers are:"+this.state.selectedcustomers);
    }
    handleselected(e){
        console.log("handle slected recieved"+e);
        this.setState({selectedcustomers:e});
    }
    renderHeader() {
        return (
            <span className="ql-formats">
                <button className="ql-bold" aria-label="Bold"></button>
                <button className="ql-italic" aria-label="Italic"></button>
                <button className="ql-underline" aria-label="Underline"></button>
            </span>
        );
    }
    save() {
        this.growl.show({severity: 'success', summary: 'Success', detail: 'Data Saved'});
    }
    async getsaveditems(){
        return await axios.get('http://localhost:9090/message', {
            params: {
              un: 'bob'
            }})
            .then(function(response){
                console.log(response.data); // ex.: { user: 'Your User'}
                console.log(response.status); // ex.: 200
                
                let results=[];// console.log(response.data[0].messageTitle);
                for(let i=0;i<response.data.length;i++){
                    console.log(response.data[i].messageTitle);
                    results.push({
                        label:response.data[i].messageTitle, 
                        icon: 'fa fa-close',
                        value:response.data[i].messageContent,
                        id:response.data[i].messageId,
                    });
                }
                return results;
            }) .catch((error)=> {
                // handle error
                // this.handleSubmit ();
                console.log("error:"+error);
                return [];
            });  
    }
    async handledelete(){
        var areyousure="";
        for(let i =0;i<this.state.selectedcustomers.length;i++){
            areyousure +=this.state.selectedcustomers[i].personId+' '+this.state.selectedcustomers[i].name+'\n';
        }
        let url = 'http://localhost:9090/person?';
        if(window.confirm("are you sure you want to delete:\n"+areyousure)){
            for(let i=0;i<this.state.selectedcustomers.length;i++){
                console.log("trying o delete personid:"+this.state.selectedcustomers[i].personId);
                await axios.delete(url,{
                    params:{personId:this.state.selectedcustomers[i].personId}
                }).then(res => {
                    console.log(res);
                  });
            }
            this.getcustomers();
        }
    }
    
    render() {
        var userboxcolor = {
            backgroundColor: "#DEEDF5",
            border:"solid",
            borderTopLeftRadius: 10,
            borderBottomLeftRadius: 10,
            paddingTop:0
          };
          var modules = {
            toolbar: [
              [{ 'header': '1'}, {'header': '2'}, { 'font': [] }],
              [{size: []}],
              ['bold', 'italic', 'underline', 'strike', 'blockquote'],
              [{'list': 'ordered'}, {'list': 'bullet'}, 
               {'indent': '-1'}, {'indent': '+1'}],
              ['link', 'image', 'video'],
              ['clean'],
              [{'direction':'rtl'}],
            ],
            clipboard: {
              // toggle to add extra line breaks when pasting HTML:
              matchVisual: false,
            }
          }
        //   const header = this.renderHeader(); 
        // const messageloadctx = [
        //     {
        //         label: 'delete'
        //     }
        // ];
        
        return (
            <div className="ui-g">

                <div className="ui-g-12 ui-md-6" style={userboxcolor}  >
                    <FormGroup>
                       <Customerresultbox searchword={this.state.searchword} HandleSelect={this.handleselected.bind(this)} HandleDelete={this.handledelete.bind(this)}
                       HandleAdd={() =>{
                           console.log(this.state.selectedcustomers.length);
                           this.setState({ show: true });
                       }} customers={this.state.customers}/>
                       <Modal show={this.state.show} onHide={this.handleHide} container={this} aria-labelledby="contained-modal-title" >
                                <Modal.Header closeButton>
                                    <Modal.Title id="contained-modal-title">
                                        Add customer
                                    </Modal.Title>
                                </Modal.Header>
                                <Modal.Body>
                                    <AddCustomer handleHide={this.handleHide.bind(this)}/>
                                </Modal.Body>
                                {/* <Modal.Footer>
                                    <Button bsStyle="primary"   >Save changes</Button>
                                </Modal.Footer> */}
                        </Modal>

                    </FormGroup>
                </div>
            
                <div className="ui-g-12 ui-md-6" style={{paddingTop:0}}>
                   <Menubar  model={this.state.items} >
                        <SplitButton label="Send" icon="pi pi-check"  model={this.state.Senditems} onClick={this.Sendemail}></SplitButton>
                    </Menubar>
                     <ReactQuill style={{background:"white"}} modules={modules}  value={this.state.text}
                  onChange={(e) =>{
                    this.setState({text: e});
                    console.log(e.htmlValue);
                    console.log("@@@@@@@@LOLOL@@@@@@@@@@@@@@@");
                    console.log(e.textValue);
                    
                    }
                    } ref={(el) => this.reactQuillRef = el}  />
                        
                    <div className="ui-g ui-fluid">
                    <Growl ref={(el) => this.growl = el}></Growl>
                        

                    </div>
                </div>
            </div>
        );
    }
}