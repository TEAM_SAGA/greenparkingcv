import React, { Component } from 'react';
import { Row, Col, Form, FormGroup, InputGroup, FormControl, Button } from 'react-bootstrap'

class Footer extends Component {
    render() {
        return (
            <div className="container footer">
                <Row>
                    <Col xs={9} className="footer_description">
                        <p>הרבה מיליםהרבה מיליםהרבה מיליםהרבה מיליםהרבה מיליםהרבה מילים</p>
                    </Col>
                    <Col xs={3}>
                        <h3 className="footer_header_description">פרטים נוספים</h3>
                    </Col>
                </Row>
                <Row>
                    <Form inline>
                        <FormGroup controlId="formInlineName">
                            <FormControl className="request_feedback" type="text" placeholder="שם מלא" />
                        </FormGroup>{' '}
                        <FormGroup controlId="formInlinePhone">
                            <FormControl className="request_feedback" type="text" placeholder="מספר טלפון" />
                        </FormGroup>{' '}
                        <FormGroup controlId="formInlineEmail">
                            <FormControl className="request_feedback" type="email" placeholder="כתובת מייל" />
                        </FormGroup>{' '}
                        <Button className="form_button" type="submit">שלח בקשה</Button>
                    </Form>
                </Row>
            </div>
        );
    }
}

export default Footer;