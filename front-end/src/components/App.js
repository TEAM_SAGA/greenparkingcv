import React, { Component } from 'react';
import '../stylesheets/App.css';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Home from '../pages/home';
import ContactUs from '../pages/contact_us';
import Gallery from '../pages/gallery';
import Admin from '../pages/admin';
import Navbar from './NavBar';
import Header from './header';
import Footer from './footer'
import cookie from 'react-cookies'

class App extends React.Component {
    constructor(){
        super();
        this.state={username:"",
                    password:"",
                    userId:"",
                };
    }
    async HandleSession(item){
        console.log("my cur state is:")
        console.log(this.state);
        await this.setState({username:item.username,password:item.password});
        console.log("now its:")
        console.log(this.state);
    }
    isconnected(){
        if(cookie.load('userId')){
            return <Route path="/admin" component={Admin} />
        }
        // console.log("not logged in Nibba!");
        return <Route path="/admin" component={Home} />;
    }
    onlogout(){
        cookie.remove("userId");
        this.setState(this.state);
    }
    onlogin(username){
        cookie.save('userId',username);
        this.setState({userId:username});
    }
  render() {
    return (
        <Router>
            <div>
                <Header Login={this.HandleSession.bind(this)} onlogin={this.onlogin.bind(this)} logout={this.onlogout.bind(this)}/>
                <Navbar />
                <Route exact path="/" component={Home} />
                <Route path="/contact_us" component={ContactUs} />
                <Route path="/gallery" component={Gallery} />
                {this.isconnected()}
                <Footer/>
            </div>
        </Router>
    );
  }
}

export default App;
