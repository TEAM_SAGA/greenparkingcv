import React from "react";
import {InputText} from 'primereact/inputtext';
import {Button} from 'primereact/button';
import {Dropdown} from 'primereact/dropdown';
import axios from 'axios';

export default class AddCustomer extends React.Component{
    constructor(){
        super();
        this.state ={
            id:"",
            role:"",
            name:"",
            lastname:"",
            phone:"",
            email:"",
            roles:[],
        }
        this.getroles = this.getroles.bind(this)
        this.handleClick = this.handleClick.bind(this)
    }
    componentDidMount() {
        this.getroles();
      }
    handleChangeEmail(event) {
        this.setState({email: event.target.value})
    }
    handleChangePhone(event) {
        this.setState({phone: event.target.value})
    }
    handleChangeName(event) {
        this.setState({name: event.target.value})
    }
    async handleClick (e) {
        e.preventDefault();
        console.log(this.state.name);
        if(!this.state.name) {
            window.alert('Please enter the name');
            return;
        }
        if(!this.state.lastname) {
            window.alert('Please enter the last name');
            return;
        }
        if(!this.state.email) {
            window.alert('Please enter the email');
            return;
        }
        if(!this.state.id) {
            window.alert('Please enter the id');
            return;
        }
        if(!this.state.phone) {
            window.alert('Please enter the phone');
            return;
        }
        if(!this.state.email) {
            window.alert('Please enter the email');
            return;
        }
        if(!this.state.role) {
            window.alert('Please Select a role');
            return;
        }
        // console.log(this.state);
        // let roles= this.getroles();
        let url = 'http://localhost:9090/person';
        let rolename ="";
        for(let i=0;i<this.state.roles.length;i++){
            if(this.state.role==this.state.roles[i].roleId){
                rolename=this.state.roles[i].roleName;
            }
        }
        await axios.post(url,{
            personId:this.state.id,
            roleName:rolename,
            name:this.state.name,
            lastName:this.state.lastname,
            email:this.state.email,
            phone:this.state.phone,
            enabled:true,
        }).then(res => {
            console.log(res);
          }).catch((error)=> {
            console.log(error);
        });  
        window.alert('Added customer');
        this.props.handleHide();
    }
    async getroles(){
        let url = 'http://localhost:9090/role';
        let rawroles= await axios.get(url).then(res => {
            // this.updateloadmessage();
            // console.log(res);
            console.log(res.data);
            this.setState({roles:res.data});
          });
    }
    render(){
        // console.log("raw is:");
        // console.log(this.state.roles);
        let roles = [];
        let width="";
        for(let i =0;i<this.state.roles.length;i++){
            if(width.length<this.state.roles[i].roleName.length){
                width=this.state.roles[i].roleName;
            }
            let newrole={label:(this.state.roles[i].roleName), value:this.state.roles[i].roleId};
            roles.push(newrole);
        }
        // roles.push({label: 'this.statasdsadasdasd', value:5},);
        // let roles2 = [
        //     {label: 'this.stat', value:1},
        //     {label: 'employee', value:2},
        //     {label: 'customer', value: 3},
        // ];
        // console.log("roles is:");console.log(roles);
        // console.log("roles2 is:");console.log(roles2);
        // console.log("width is:"+width);
        return(
            <div>
                
                <div className="content-section implementation">
                    <div className="ui-g ui-fluid">
                        <form>

                            <div className="ui-g-12 ui-md-6 ui-lg-4">
                                <span>ID: </span>
                                <InputText   placeholder="313783912" onChange={(e) => this.setState({id: e.target.value})}></InputText>
                            </div>
                            <div className="ui-g-12 ui-md-6 ui-lg-4">
                                <span> Name: </span>
                                <InputText  placeholder="Eric Freeman" onChange={(e) => this.setState({name: e.target.value})}></InputText>
                            </div>
                            <div className="ui-g-12 ui-md-6 ui-lg-4">
                                <span> lastname: </span>
                                <InputText placeholder="Eric Freeman" onChange={(e) => this.setState({lastname: e.target.value})}></InputText>
                            </div>
                            <div className="ui-g-12 ui-md-6 ui-lg-4">
                                <span>Phone: </span>
                                <InputText  placeholder="050-582-9715"  onChange={(e) => this.setState({phone: e.target.value})}></InputText>
                            </div>
                            <div className="ui-g-12 ui-md-6 ui-lg-4">
                                <span>email: </span>
                                <InputText  placeholder="example@gmail.com"  onChange={(e) => this.setState({email: e.target.value})}></InputText>
                            </div>
                            {/* birthday below! */}
                            {/* <div className="ui-g-12 ui-md-6 ui-lg-4">
                                <span>Birthday: {this.state.val4}</span>
                                <InputText  value={this.state.val4} placeholder="11/1/1996" onChange={(e) => this.setState({val4: e.value})}></InputText>
                            </div> */}
                            <div className="ui-g-12">
                                <hr style={{margin:0}}/>
                            </div>
                            <div className="ui-g-12" style={{display:'flex', justifyContent: 'space-between',margin:0}}>
                                <div className="ui-g-12 ui-md-6 ui-lg-4" >
                                    
                                <Dropdown value={this.state.role} options={roles} onChange={(e) => {this.setState({role: e.value})}} placeholder="Select a role"
                                required={true} autoWidth={false} style={{width:("Select a role ".length+"ch")}}/>
                                </div>
                                <div className="ui-g-12 ui-md-6 ui-lg-4" >
                                    
                                    <Button label="Save" onClick={this.handleClick}/>
                                </div>
                            </div>
                            </form>
                        
                        {/* <div className="ui-g-12 ui-md-6 ui-lg-4">
                            <span>Phone Ext: {this.state.val5}</span>
                            <InputText  value={this.state.val5} placeholder="(999) 999-9999? x99999" onChange={(e) => this.setState({val5: e.value})}></InputText>
                        </div>

                        <div className="ui-g-12 ui-md-6 ui-lg-4">
                            <span>Serial Number: {this.state.val6}</span>
                            <InputText  value={this.state.val6} placeholder="a*-999-a999" onChange={(e) => this.setState({val6: e.value})}></InputText>
                        </div> */}
                    </div>
                </div>
            </div>

        )
    }
}