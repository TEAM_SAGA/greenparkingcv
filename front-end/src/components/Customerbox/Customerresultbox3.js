import React from "react";
import {DataTable} from 'primereact/datatable';
import {Column} from 'primereact/column';
import 'primereact/resources/themes/omega/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import {Button} from 'primereact/button';
import {InputText} from 'primereact/inputtext';
import {Growl} from 'primereact/growl';
export default class Customerresultbox3 extends React.Component{
  constructor(props) {
    super(props);
    this.state = {customers:[]};
    this.requiredValidator = this.requiredValidator.bind(this);
    this.emailEditor = this.emailEditor.bind(this);
    this.phoneEditor = this.phoneEditor.bind(this);
  //  this.filterselect = this.filterselect.bind(this);
    this.componentDidMount = this.componentDidMount.bind(this); 
}
componentDidMount() {
  // console.log("comps");
  // console.log(this.props.Customers)
  // this.setState({customers:this.props.Customers});
  this.setState({results:this.getresults()});
}
componentWillReceiveProps(props){
  console.log("propsorops");
  console.log(props);
  this.setState({customers:props.customers});
}
getresults=()=>{
  var results = [ 
    {id:"313313992", name:"052",lastname:"s",email:"asd@asd.asd",phone:"123"},
    {id:"313313993", name:"053",lastname:"d",email:"asd@asd.asd",phone:"123"},
    {id:"313313994", name:"054",lastname:"bob",email:"asd@asd.asd",phone:"123"},
    {id:"313313996", name:"055",lastname:"bob",email:"asd@asd.asd",phone:"123"},
    {id:"313313997", name:"055",lastname:"bob",email:"asd@asd.asd",phone:"123"},
    {id:"313313998", name:"055",lastname:"bob",email:"asd@asd.asd",phone:"123"},
    {id:"313313999", name:"055",lastname:"bob",email:"asd@asd.asd",phone:"123"},
    {id:"313313971", name:"055",lastname:"bob",email:"asd@asd.asd",phone:"123"},
    {id:"313313972", name:"04",lastname:"bob",email:"asd@asd.asd",phone:"123"},
    {id:"313313973", name:"04",lastname:"bob",email:"asd@asd.asd",phone:"123"},
    {id:"313313974", name:"04",lastname:"bob",email:"asd@asd.asd",phone:"123"},
    {id:"313313985", name:"04",lastname:"bob",email:"asd@asd.asd",phone:"123"},
    {id:"313313986", name:"04",lastname:"bob",email:"asd@asd.asd",phone:"123"},
    {id:"313313987", name:"312",lastname:"bob",email:"asd@asd.asd",phone:"123"},
    {id:"313313988", name:"312",lastname:"bob",email:"asd@asd.asd",phone:"123"},
    {id:"313313989", name:"055",lastname:"bob",email:"asd@asd.asd",phone:"123"},
    {id:"313313990", name:"0",lastname:"bob",email:"asd@asd.asd",phone:"123"},
    {id:"313313913", name:"0552",lastname:"bob",email:"asd@asd.asd",phone:"123"},
    {id:"313313914", name:"0553",lastname:"bob",email:"asd@asd.asd",phone:"123"},
    {id:"313313915", name:"0554",lastname:"bob",email:"asd@asd.asd",phone:"123"},
    {id:"313313916", name:"0555",lastname:"bob",email:"asd@asd.asd",phone:"123"},

      ];
    return results;
}
displaySelection(data) {
  if(!data || data.length === 0) {
      return <div style={{textAlign: 'left'}}>No Selection</div>;
  }
  else {
      if(data instanceof Array){
        
        // return <ul style={{textAlign: 'left', margin: 0}}>{data.map((car,i) => <li key={car.id}>{car.id + ' - ' + car.name}</li>)}</ul>;
        return  <div align='left'>
          <Button className="ui-button-danger"  icon="pi pi-times-circle" label="Delete"  onClick={this.props.HandleDelete}/>
          </div>
      }
      // else
      //     return <div style={{textAlign: 'left'}}>Selected Customer: {data.vin + ' - ' + data.year + ' - ' + data.brand + ' - ' + data.color}</div>
  }
}
onEditorValueChange(props, value,firstvalue) {
  let updatedCars = [...props.value];
  updatedCars[props.rowIndex][props.field] = value;
  this.setState({results: updatedCars});
  console.log("Value changed for id:"+props.rowData.id+ " from "+firstvalue+" to "+value);
  console.log("add axios here, edit the id to new value");
  this.growl.show({severity: 'success', summary: 'Success Message', detail: "Value changed for id:"+props.rowData.id+ " from "+firstvalue+" to "+value});
  this.growl.show({severity: 'error', summary: 'Error Message', detail: 'Not implemented'});
}
inputTextEditor(props, field,firstvalue) {
  return <form>
    <InputText type="text" defaultValue={firstvalue} 
    onChange={(e) =>{
    } 
    } 
    onKeyDown={(e)=>{
      if(e.keyCode===13){
        this.onEditorValueChange(props, e.target.value,firstvalue);
      }
    }}
    />
    </form>
}
emailEditor(props) {
  console.log(props);
  console.log("in emailEditor")
  return this.inputTextEditor(props, 'email',props.rowData.email);
}
phoneEditor(props) {
  console.log(props);
  console.log("in phoneEditor")
  return this.inputTextEditor(props, 'phone',props.rowData.phone);
}
requiredValidator(props) {
  let value = props.rowData[props.field];
  return value && value.length > 0;
}


    render() {
      console.log(this.props.customers);  
      var header = <div className="ui-g">
                        <div className="ui-g-4" style={{display:'flex',justifyContent:'flex-start'}}>

                        <Button  icon="pi pi-plus" label="הוסף" onClick={this.props.HandleAdd}/>
                        </div>
                      <div className="ui-g-8" style={{display:'flex',justifyContent:'flex-end'}}>
                        <InputText ref={(el) => this.st = el} type="search" onInput={(e) =>{
                          console.log("INPUTTED");
                          this.setState({globalFilter: e.target.value});
                          let data =this.dt.props.selection;
                          let filter=e.target.value;
                          let asd=[];
                          if(filter!=null&&data!=null){
                            for(let i =0;i<data.length;i++){
                              if(data[i].id.toString().includes(filter)||data[i].name.toString().includes(filter)||data[i].email.toString().includes(filter)||data[i].lastname.toString().includes(filter)||data[i].phone.toString().includes(filter))
                              asd.push(data[i]);
                            }
                          }
                          else {
                            if(data!=null)
                            asd=data;
                          }
                          console.log("FINAL NUMBER LEN IS:"+asd.length+"for:"+filter+" e.asd="+e.target.value);
                          console.log(this.state.globalFilter );
                          this.props.HandleSelect(asd);
                        }} placeholder="חיפוש גלובלי" />

                        </div>
                    </div>;  
        return (
          <div>
            <Growl ref={(el) => this.growl = el} />
              <DataTable value={this.state.customers}  footer={this.displaySelection(this.state.selectedCars3)}
                        selection={this.state.selectedCars3} onSelectionChange={(e) => {
                          let filter=this.dt.props.globalFilter;
                          let asd=[];
                          if(filter!=null){
                            for(let i =0;i<e.data.length;i++){
                              if(e.data[i].id.toString().includes(filter)||e.data[i].name.toString().includes(filter)||e.data[i].email.toString().includes(filter)||e.data[i].lastname.toString().includes(filter)||e.data[i].phone.toString().includes(filter))
                              asd.push(e.data[i]);
                            }
                          }
                          else {
                            asd=e.data;
                            }
                          this.setState({selectedCars3: e.data});
                          console.log("e.data.length is:"+e.data.length+" and this.dt.props is:");
                          console.log(this.dt);
                          this.props.HandleSelect(asd);
                        }} scrollable={true} scrollHeight="350px"
                        header={header}  globalFilter={this.state.globalFilter} ref={(el) => this.dt = el} editable={true}>

                <Column selectionMode="multiple" style={{width:'2em'}}/>
                <Column field="phone" header="ט''ל" sortable={true} editor={this.phoneEditor} editorValidator={this.requiredValidator} style={{overflow:'auto'}}/>
                <Column field="email" header="דוא''ל" sortable={true} editor={this.emailEditor} editorValidator={this.requiredValidator} style={{overflow:'auto'}}/>
                <Column field="lastName" header="שם משפחה" sortable={true} style={{textAlign:'right',overflow:'auto'}}/>
                <Column field="name" header="שם" sortable={true} style={{textAlign:'right',overflow:'auto'}} />
                <Column field="personId" header="ת''ז" sortable={true} style={{overflow:'auto'}}/>
              </DataTable>
            </div>
        );
    }
}