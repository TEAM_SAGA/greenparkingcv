import React, { Component } from 'react'
import { Navbar, Nav, NavItem, FormGroup, FormControl, Button} from 'react-bootstrap'
import { Link } from 'react-router-dom'
import { Redirect } from 'react-router'
import axios from 'axios';
import cookie from 'react-cookies'
import Admin from '../pages/admin';
export default class NavBar extends Component {
    constructor(){
        super();
        this.state={
            username:'asd',
            password:'',
            redirect: false,
        }
        this.handleSubmit.bind(this);
        this.isadmin.bind(this);
    }
    isadmin(){
        if(cookie.load('userId'))
        {
            return <NavItem eventKey={4} componentClass={Link} href="/admin" to="/admin">
                        אדמין
                    </NavItem>
        }
        return null;
    }
    handleSubmit () {
        console.log("now true");
        this.setState({ redirect: true });
      }
    render() {
        if(this.state.redirect){
            console.log("IS REDIRECTIN");
            return <Redirect to='/admin' Component={Admin}/>;
        }
        return (
            <Navbar default collapseOnSelect className="container">
                <Navbar.Collapse>
                    <Nav pullRight>
                        {this.isadmin()}
                        <NavItem eventKey={3} componentClass={Link} href="gallery" to="/gallery">
                            גלרייה
                        </NavItem>
                        <NavItem eventKey={2} componentClass={Link} href="/contact_us" to="/contact_us">
                            קצת עלינו
                        </NavItem>
                        <NavItem eventKey={1} componentClass={Link} href="/" to="/">
                            עמוד הבית
                        </NavItem>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        );
    }
}
/*<Navbar.Header>
    <Navbar.Brand>
        <Link to="/">Green Parking</Link>
    </Navbar.Brand>
    <Navbar.Toggle />
</Navbar.Header>*/
/*<Navbar.Form pullLeft>
    <FormGroup>
        <FormControl type="text" name="username" placeholder="Username" onChange={(e)=>{
            this.setState({username:e.target.value});
        }} />
    </FormGroup>{' '}
    <FormGroup>
        <FormControl type="password" name="password" placeholder="Password"  onChange={(e)=>{
            this.setState({password:e.target.value});
        }} />
    </FormGroup>{' '}*/
    {/* <Button type="submit" eventKey={4} componentClass={Link} href="/admin" to="/admin">Log in</Button> */}
    /*<Button type="submit" eventKey={4} omponentClass={Link}  onClick={(e)=>{
        console.log("username:"+this.state.username);
        console.log("passowrd:"+this.state.password);
        console.log("username:"+this.state.username+" "+"password:"+this.state.password);
        axios.post('http://localhost:9090/person/login',{
            username:this.state.username,
            password:this.state.password
        })
            .then((response)=> {
                // handle success
                this.handleSubmit ();
                console.log("success:"+response);
            })
            .catch((error)=> {
                // handle error
                // this.handleSubmit ();
                console.log(error);
            });
    }} >Log in</Button>
</Navbar.Form>*/