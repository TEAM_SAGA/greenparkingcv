# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table images_tbl (
  imageid                       integer auto_increment not null,
  url                           varchar(255),
  description                   varchar(255),
  constraint pk_images_tbl primary key (imageid)
);

create table messages_tbl (
  messageid                     integer auto_increment not null,
  messagetitle                  varchar(255),
  messagecontent                varchar(255),
  constraint pk_messages_tbl primary key (messageid)
);

create table person_tbl (
  personid                      integer auto_increment not null,
  roleid                        integer,
  name                          varchar(255),
  username                      varchar(255),
  password                      varchar(255),
  lastname                      varchar(255),
  email                         varchar(255),
  phone                         varchar(255),
  createdat                     datetime(6),
  updatedat                     datetime(6),
  enabled                       tinyint(1) default 0,
  constraint pk_person_tbl primary key (personid)
);

create table roles_tbl (
  roleid                        integer auto_increment not null,
  rolename                      varchar(255),
  constraint pk_roles_tbl primary key (roleid)
);

alter table person_tbl add constraint fk_person_tbl_roleid foreign key (roleid) references roles_tbl (roleid) on delete restrict on update restrict;
create index ix_person_tbl_roleid on person_tbl (roleid);


# --- !Downs

alter table person_tbl drop foreign key fk_person_tbl_roleid;
drop index ix_person_tbl_roleid on person_tbl;

drop table if exists images_tbl;

drop table if exists messages_tbl;

drop table if exists person_tbl;

drop table if exists roles_tbl;

