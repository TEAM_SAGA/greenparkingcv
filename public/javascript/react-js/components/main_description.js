import React from 'react'
import { Row, Col } from 'react-bootstrap'

function MainDescription(props) {
    return (
        <Col >
            <Row className="show-grid">
                <Col>
                    <h1>{props.customHeader}</h1>
                </Col>
            </Row>

            <Row className="show-grid">
                <Col>
                    <p>{props.customDescription}</p>
                </Col>
            </Row>
        </Col>
    );
}

export default MainDescription