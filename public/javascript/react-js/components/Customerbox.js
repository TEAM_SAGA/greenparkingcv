import React from "react";
import { ButtonToolbar,Modal,Col,FormGroup, Button, MenuItem,DropdownButton } from 'react-bootstrap';
import Customerresultbox from '../components/Customerbox/Customerresultbox3';
import {Editor} from 'primereact/editor';
import {Growl} from 'primereact/growl';
import {SplitButton} from 'primereact/splitbutton';
import 'primereact/resources/themes/omega/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import {Button as PButton} from 'primereact/button';
import {Menubar} from 'primereact/menubar';
import {InputText} from "primereact/inputtext";
import {Menu} from 'primereact/menu';
import AddCustomer from '../components/Customerbox/AddCustomer';
import ReactQuill from 'react-quill'; // ES6
import 'react-quill/dist/quill.snow.css'; // ES6
export default class Costumerbox extends React.Component{
    constructor(){
        super();
        this.handleHide = this.handleHide.bind(this);
        this.quillRef = null;      // Quill instance
        this.reactQuillRef = null; // ReactQuill component
        this.state={
            searchword:"",
            selectedcustomers:[],
            show: false,
            text:"",
            Senditems: [
                {
                    label: 'SMS', 
                    icon: 'fa fa-refresh', 
                    command: (e) => {
                        let message = this.handletext(this.state.text,true);
                        this.growl.show({severity:'success', summary:'Sent', detail:message});
                        this.Sendsms(message);
                    
                    }
                },
                {
                    label: 'EMAIL', 
                    icon: 'fa fa-close',
                    command: (e) => {
                        let message = this.handletext(this.state.text,false);
                        this.Sendemail(message);
                        this.growl.show({ severity: 'success', summary: 'Sent', detail:message });
                    
                    }
                },

            ],
            items: [{
                label: 'File',
                icon: 'fa fa-fw fa-file-o',
                items: [{
                    label: 'Save custom message',
                    icon: 'pi pi-save',
                    command: (e) => {
                        // this.Sendsms();
                        this.growl.show({severity:'success', summary:'Saved', detail:"Successful save"});
                    
                    }
                },
                {
                    label: 'Load ',
                    items:this.getsaveditems(),
                    icon: 'pi pi-upload',
                },
                ]
            }],
            
        };
        this.getsaveditems.bind(this);
        this.save = this.save.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handletext= this.handletext.bind(this);
    }
    componentDidMount() {
        this.attachQuillRefs()
      }
      
      componentDidUpdate() {
        this.attachQuillRefs()
      }
      
      attachQuillRefs = () => {
        if (typeof this.reactQuillRef.getEditor !== 'function') return;
        this.quillRef = this.reactQuillRef.getEditor();
      }
    Sendemail(e){
        console.log("send email not implemented yet");
    }
    Sendsms(e){
        console.log("send sms not implemented yet");
    }
    handletext(e,forsms){
        // if(e!=null&&e!="")
        //     {
        //         if(forsms)
        //             return e.textValue;
        //         else return e.htmlValue;
        //     }
        // return "";
        const editor = this.reactQuillRef.getEditor();
        const unprivilegedEditor = this.reactQuillRef.makeUnprivilegedEditor(editor);
        if(e!=null&&e!=""&&this.quillRef!=null){
            return (forsms)?unprivilegedEditor.getText():unprivilegedEditor.getHTML();
        return ""
        }
    }
    handleChange(value) {
        this.setState({ text: value })
        console.log(value);
      }
    handleHide() {
        this.setState({ show: false });
      }
    handlesearchchange(e){
        console.log(e.target.value);
    }
    handleClick(e){ 
        let selectedstring="";
        for(let i = 0;i<this.state.selectedcustomers.length;i++){
            console.log("i is:"+i+this.state.selectedcustomers[i].name+" "+this.state.selectedcustomers[i].id);
            selectedstring+=this.state.selectedcustomers[i].name+" "+this.state.selectedcustomers[i].id+"\n";
        }
        alert(selectedstring);
        // alert("The selected customers are:"+this.state.selectedcustomers);
    }
    handleselected(e){
        console.log("handle slected recieved"+e);
        this.setState({selectedcustomers:e});
    }
    renderHeader() {
        return (
            <span className="ql-formats">
                <button className="ql-bold" aria-label="Bold"></button>
                <button className="ql-italic" aria-label="Italic"></button>
                <button className="ql-underline" aria-label="Underline"></button>
            </span>
        );
    }
    save() {
        this.growl.show({severity: 'success', summary: 'Success', detail: 'Data Saved'});
    }
    getsaveditems(){
        let savedprefixes = [
            {
                label: 'Touch me senpai ',
                icon: 'pi pi-save',
                value:'<p>yes sir</p>',
                command: (e) => {
                    // this.Sendsms();
                    this.growl.show({severity:'success', summary:'Loaded', detail:"Successful load"});
                    console.log(e.item.value);
                    this.setState((prevState, props) => ({
                        text: e.item.value
                      }));
                    //   console.log("changing this.state.text to"+e.item.value);
                    // console.log(this.editor.value);  
                }
            },
            {
                label: 'yes senpai ',
                icon: 'pi pi-save',
                value:'כן אבא',
            },

        ];
        var myjson = JSON.stringify(savedprefixes);
        var asd = JSON.parse(myjson);
        console.log(asd);
        asd[0].command=(e) => {
            // this.Sendsms();
            this.growl.show({severity:'success', summary:'Loaded', detail:"sexy man! load"});
            console.log(e.item.value);
            this.setState((prevState, props) => ({
                text: e.item.value
              }));
            //   console.log("changing this.state.text to"+e.item.value);
            // console.log(this.editor.value);  
        }
        asd[1].command=(e) => {
            // this.Sendsms();
            this.growl.show({severity:'success', summary:'Loaded', detail:"sexy man! load"});
            console.log(e.item.value);
            this.setState((prevState, props) => ({
                text: e.item.value
              }));
            //   console.log("changing this.state.text to"+e.item.value);
            // console.log(this.editor.value);  
        }

        return asd;
    }
    handledelete(){
        var areyousure="";
        for(let i =0;i<this.state.selectedcustomers.length;i++){
            areyousure +=this.state.selectedcustomers[i].id+' '+this.state.selectedcustomers[i].name+'\n';
        }
        window.confirm("are you sure you want to delete:\n"+areyousure);
    }
    render() {
        var userboxcolor = {
            backgroundColor: "#DEEDF5",
            border:"solid",
            borderTopLeftRadius: 10,
            borderBottomLeftRadius: 10,
          };
          var modules = {
            toolbar: [
              [{ 'header': '1'}, {'header': '2'}, { 'font': [] }],
              [{size: []}],
              ['bold', 'italic', 'underline', 'strike', 'blockquote'],
              [{'list': 'ordered'}, {'list': 'bullet'}, 
               {'indent': '-1'}, {'indent': '+1'}],
              ['link', 'image', 'video'],
              ['clean'],
              [{'direction':'rtl'}],
            ],
            clipboard: {
              // toggle to add extra line breaks when pasting HTML:
              matchVisual: false,
            }
          }
          const header = this.renderHeader(); 
        return (
            <div  >
                <Col md={this.props.size} style={userboxcolor}  >
                    <FormGroup>
                       <Customerresultbox searchword={this.state.searchword} HandleSelect={this.handleselected.bind(this)} HandleDelete={this.handledelete.bind(this)}
                       HandleAdd={() =>{
                           console.log(this.state.selectedcustomers.length);
                           this.setState({ show: true });
                       } 
                       }/>
                       <Modal show={this.state.show} onHide={this.handleHide} container={this} aria-labelledby="contained-modal-title">
                                <Modal.Header closeButton>
                                    <Modal.Title id="contained-modal-title">
                                        Add customer
                                    </Modal.Title>
                                </Modal.Header>
                                <Modal.Body>
                                    <AddCustomer/>
                                </Modal.Body>
                                <Modal.Footer>
                                    <Button bsStyle="primary">Save changes</Button>
                                </Modal.Footer>
                        </Modal>
                       {/* <Col md={12}>
                       <ButtonToolbar style={{margin:15}}>
                            <Button bsStyle="danger" bsSize="medium" className="pull-left" >
                                Delete Customer
                            </Button>
                            <Button bsSize="xs" className="center" onClick={this.handleClick.bind(this)} >
                                test
                            </Button>
                            <Button bsStyle="success" bsSize="large" onClick={() => this.setState({ show: true })}>
                                Add customer
                            </Button>

                            <Modal show={this.state.show} onHide={this.handleHide} container={this} aria-labelledby="contained-modal-title">
                                <Modal.Header closeButton>
                                    <Modal.Title id="contained-modal-title">
                                        Add customer
                                    </Modal.Title>
                                </Modal.Header>
                                <Modal.Body>
                                    <AddCustomer/>
                                </Modal.Body>
                                <Modal.Footer>
                                    <Button bsStyle="primary">Save changes</Button>
                                </Modal.Footer>
                            </Modal>
                            </ButtonToolbar>;
                        </Col> */}
                    </FormGroup>
                </Col>
            
                <Col md={12 - this.props.size}>
                   <Menubar  model={this.state.items}>
                        <SplitButton label="Send" icon="pi pi-check"  model={this.state.Senditems}></SplitButton>
                    </Menubar>
                    {/* <Editor value={""} onTextChange={(e) =>{
                    this.setState({text: e});
                    console.log(e.htmlValue);
                    console.log("@@@@@@@@LOLOL@@@@@@@@@@@@@@@");
                    console.log(e.textValue);
                    
                    }
                    } ref={(el) => this.editor = el} /> */}
                     <ReactQuill modules={modules}  value={this.state.text}
                  onChange={(e) =>{
                    this.setState({text: e});
                    console.log(e.htmlValue);
                    console.log("@@@@@@@@LOLOL@@@@@@@@@@@@@@@");
                    console.log(e.textValue);
                    
                    }
                    } ref={(el) => this.reactQuillRef = el}  />
                        
                    <div className="ui-g ui-fluid">
                    <Growl ref={(el) => this.growl = el}></Growl>
                        <div className="ui-g-2">
                            <PButton label="Clear" icon="pi pi-times" onClick={()=> this.setState({text:''})}/>
                            <InputText value={this.state.text}  />
                        </div>
                        {/* <div class="ui-g-2">
                            <PButton label="Save as" icon="pi pi-plus"/>
                        </div>
                        <div class="ui-g-2">
                            <SplitButton label="Save" icon="pi pi-check"  model={this.state.items}></SplitButton>
                        </div>
                        <div class="ui-g-4"/>
                        <div class="ui-g-2" >
                            
                            <SplitButton label="Send" icon="pi pi-check"  model={this.state.Senditems}></SplitButton>
                        </div> */}
                        

                    </div>
                </Col>
            </div>
        );
    }
}