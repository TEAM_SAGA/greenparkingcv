import React from "react";
import {Col, Row} from "react-bootstrap";
import {SocialIcon} from "react-social-icons";

export default class ContactUsAdvert extends React.Component {
    render() {
        return (
            <Col xsOffset={1} xs={5}>
                <Row>
                    <Col>
                        <div><h2>General Contact</h2>
                            <p className="Accordion-content">Phone: +986-8543683</p>
                            <p className="Accordion-content">Email: admin@fruitproducts.com</p>
                            <p className="Accordion-content">Our social media:</p>
                        </div>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <SocialIcon url="http://twitter.com/jaketrent" />
                        <SocialIcon url="https://www.facebook.com/bananainfo/"/>
                    </Col>
                </Row>

            </Col>
        );
    }
}