import React from "react";
import {Col, Row} from "react-bootstrap";

export default class ContactForm extends React.Component{
    constructor(){
        super();
        this.state ={
            name:"",
            phone:"",
            email:"",
            content:"",
        }
        this.handleClick = this.handleClick.bind(this)
    }
    handleChangeEmail(event) {
        this.setState({email: event.target.value})
    }
    handleChangePhone(event) {
        this.setState({phone: event.target.value})
    }
    handleChangeName(event) {
        this.setState({name: event.target.value})
    }
    handleChangeContent(event) {
        this.setState({content: event.target.value})
    }
    handleClick (e) {
        e.preventDefault();
        if(!this.state.email) {
            window.alert('Please enter your email');
            return;
        }
        if(!this.state.content) {
            window.alert('Please enter your comment');
            return;
        }

        let support={
            name:this.state.name,
            email:this.state.email,
            phone:this.state.phone,
            content:this.state.content
        }
        let supportJSON = JSON.stringify(support);
        console.log(supportJSON);
        this.setState({email: ""});
        this.setState({name: ""});
        this.setState({phone: ""});
        this.setState({content: ""});
        document.getElementById("create-course-form").reset();
        window.alert('Message sent');
    }
    render(){
        return(

            <Col xs={6}>
                <Row className="show-grid">
                    <form id="create-course-form">
                        <h2>Contact us</h2>
                        <div className="input-group">
                            <div className="input-group-addon"><span
                                className="glyphicon glyphicon-envelope"></span></div>
                            <input type="text"  className="form-control" value={this.state.title} onChange={this.handleChangeEmail.bind(this)}
                                   placeholder={"Enter your email"}></input>
                        </div>

                        <div className="input-group">
                            <div className="input-group-addon"><span
                                className="glyphicon glyphicon-phone"></span></div>
                            <input placeholder={"Enter your phone(optional)"} className="form-control" type="text" value={this.state.title} onChange={this.handleChangePhone.bind(this)} ></input>
                        </div>

                        <div className="input-group">
                            <div className="input-group-addon"><span
                                className="glyphicon glyphicon-user"></span></div>
                            <input placeholder={"Enter your name(optional)"} className="form-control" type="text" value={this.state.title} onChange={this.handleChangeName.bind(this)} ></input>
                        </div>
                        <h1></h1>
                        <div className="form-group">
                            <textarea className="form-control" rows="5" id="comment" placeholder={"Enter your comment"} value={this.state.title} onChange={this.handleChangeContent.bind(this)}></textarea>
                            <button className="btn btn-lg" onClick={this.handleClick}  >Submit</button>
                        </div>
                    </form>
                </Row>
            </Col>
        )
    }
}