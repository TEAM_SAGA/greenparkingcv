import React, { Component } from 'react'
import { Row, Col, Grid } from 'react-bootstrap'

function SideDescription(props) {
    return (
        <Col xsOffset={props.customOffset} xs={props.customColSize}>
            <Row>
                <Col>
                    <h1>{props.customHeader}</h1>
                </Col>
            </Row>
            <Row>
                <Col>
                    <p>{props.customDescription}</p>
                </Col>
            </Row>
        </Col>
    );
}

export default SideDescription