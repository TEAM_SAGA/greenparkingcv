import React, { Component } from 'react';
import '../stylesheets/App.css';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Home from '../pages/home';
import ContactUs from '../pages/contact_us';
import Gallery from '../pages/gallery';
import Admin from '../pages/admin';
import Navbar from './NavBar';

class App extends Component {
  render() {
    return (
        <Router>
            <div>
                <Navbar />
                <Route exact path="/" component={Home} />
                <Route path="/contact_us" component={ContactUs} />
                <Route path="/gallery" component={Gallery} />
                <Route path="/admin" component={Admin} />
            </div>
        </Router>
    );
  }
}

export default App;
