import React, { Component } from 'react'
import { Navbar, Nav, NavItem, FormGroup, FormControl, Button} from 'react-bootstrap'
import { Link } from 'react-router-dom'

export default class NavBar extends Component {
    render() {
        return (
            <Navbar default collapseOnSelect>
                <Navbar.Header>
                    <Navbar.Brand>
                        <Link to="/">Green Parking</Link>
                    </Navbar.Brand>
                    <Navbar.Toggle />
                </Navbar.Header>
                <Navbar.Collapse>
                    <Nav pullLeft>
                        <NavItem eventKey={1} componentClass={Link} href="/" to="/">
                            Home
                        </NavItem>
                        <NavItem eventKey={2} componentClass={Link} href="/contact_us" to="/contact_us">
                            contact us
                        </NavItem>
                        <NavItem eventKey={3} componentClass={Link} href="gallery" to="/gallery">
                            gallery
                        </NavItem>
                    </Nav>
                    <Navbar.Form pullRight>
                        <FormGroup>
                            <FormControl type="text" name="username" placeholder="Username" />
                        </FormGroup>{' '}
                        <FormGroup>
                            <FormControl type="password" name="password" placeholder="Password" />
                        </FormGroup>{' '}
                        <Button type="submit" eventKey={4} componentClass={Link} href="/admin" to="/admin">Log in</Button>
                    </Navbar.Form>
                </Navbar.Collapse>
            </Navbar>
        );
    }
}