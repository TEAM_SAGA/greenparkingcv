import React from "react";
import {Col,Table, Glyphicon,Checkbox } from 'react-bootstrap';
import BootstrapTable from 'react-bootstrap-table-next';
import filterFactory, { textFilter,dateFilter  } from 'react-bootstrap-table2-filter';
import ReactDataGrid from 'react-data-grid';
export default class Customerresultbox extends React.Component{
    constructor(){
        super();
        this.state={checkedresults:[],}
      }
      handlecheckbox(){
        return 15;
      }
      render() {
        console.log("recieved this.props.selectedcustomers:"+this.props.selectedcustomers);
        console.log(this.props.searchword);
        this.state.checkedresults=this.props.selectedcustomers;
        // var userboxcolor = {
        //     backgroundColor: "#DEEDF5",
        //     border:"solid",
        //     borderTopLeftRadius: 10,
        //     borderBottomLeftRadius: 10,
        //   }
        // var searchresultbox = {
        //     backgroundColor: "#ABB8C3",
        //     border:"solid",
        //     marginTop:10,
        //     borderWidth:1,

        //   }
        //   var searchresult = {
        //     backgroundColor: "#ABB8C3",
        //     border:"solid",
        //     borderWidth:1,
        //   }
        //   var asd= function(weep){
        //       console.log(weep);
        //     var searchresult = {
        //         backgroundColor: "#ABB8C3",
        //         border:"solid",
        //         borderWidth:1,
        //       }  
        //       var searchresult2 = {
        //         backgroundColor: "#C5C6C6",
        //         border:"solid",
        //         borderWidth:1,
        //       } 
        //     return (weep%2==0)?searchresult:searchresult2;
        // }
        const products = [  ];
          const columns = [{
            dataField: 'id',
            text: 'Customer ID',
            sort: true,
            sortFunc: (a, b, order, dataField) => {
              if (order === 'asc') {
                return b - a;
              }
              return a - b; // desc
            },
          }, {
            dataField: 'name',
            text: 'Customer Name',
            filter: textFilter(),
            sort: true,        
            sortFunc: (a, b, order, dataField) => {
              if (order === 'asc') {
                return b - a;
              }
              return a - b; // desc
            },    
          }, {
            dataField: 'email',
            text: 'Customer email',
            filter: textFilter(),
            sort: true,
            sortFunc: (a, b, order, dataField) => {
              if (order === 'asc') {
                return b - a;
              }
              return a - b; // desc
            },
          },{
            dataField: 'phone',
            text: 'Phone number',
            filter: textFilter(),
            sort: true,
            sortFunc: (a, b, order, dataField) => {
              if (order === 'asc') {
                return b - a;
              }
              return a - b; // desc
            },
          },{
            dataField: 'birthday',
            text: 'Birthday date',
            filter: dateFilter(),
            sort: true,
            onSort: (a, b, order, dataField) => {
              if (order === 'asc') {
                return b - a;
              }
              return a - b; // desc
            },
          }];
          var results = [{id:"313313991", name:"050",email:"asd@asd.asd",phone:123}
          ,{id:"313313983", name:"051",email:"asd",phone:123}
          ,{id:"313313984", name:"052",email:"asd",phone:123}
          ,{id:"313313985", name:"053",email:"asd",phone:123}
          ,{id:"313313986", name:"054",email:"asd",phone:123}
          ,{id:"313313987", name:"055",email:"asd",phone:123}
          ,{id:"313313988", name:"056",email:"asd",phone:123}
          ,{id:"313313989", name:"050",email:"asd",phone:123}
          ,{id:"313313990", name:"050",email:"asd",phone:123}
          ,{id:"313313971", name:"050",email:"asd",phone:123}
          ,{id:"313313972", name:"050",email:"asd",phone:123}
          ,{id:"313313973", name:"050",email:"asd",phone:123}
          ,{id:"313313974", name:"050",email:"asd",phone:123}
          ,{id:"1", name:"050",email:"asd",phone:123}
          ,{id:"2", name:"050",email:"asd",phone:123}
          ,{id:"3", name:"050",email:"asd",phone:123}
          ,{id:"4", name:"050",email:"asd",phone:123}
          ,{id:"5", name:"050",email:"asd",phone:123}
          ,{id:"6", name:"050",email:"asd",phone:123}
          ,{id:"7", name:"050",email:"asd",phone:123}
          ,{id:"8", name:"050",email:"asd",phone:123}
          ,{id:"9", name:"050",email:"asd",phone:123}
          ,{id:"10", name:"050",email:"asd",phone:123}
          ,{id:"11", name:"050",email:"asd",phone:123}
          ,{id:"12", name:"050",email:"asd",phone:123}
          ,{id:"13", name:"050",email:"asd",phone:123}
          ,{id:"14", name:"050",email:"asd",phone:123}
          ,{id:"15", name:"050",email:"asd",phone:123}
          ,{id:"16", name:"050",email:"asd",phone:123}
          ,{id:"17", name:"050",email:"asd",phone:123}
          ,{id:"18", name:"050",email:"asd",phone:123}
          ,{id:"19", name:"050",email:"asd",phone:123}
          ,{id:"20", name:"050",email:"asd",phone:123}
          ,{id:"21", name:"050",email:"asd",phone:123}
          ,{id:"22", name:"050",email:"asd",phone:123}
          ,{id:"23", name:"050",email:"asd",phone:123}
          ,{id:"24", name:"050",email:"asd",phone:123}

            ];
          console.log(results);
          const rowEvents = {
            onClick: (e, row, rowIndex) => {
            //   console.log(row);
            //   console.log(rowIndex);

            }
          };
          var selectedRows=[];
          const selectRow = {
            mode: 'checkbox',
            clickToSelect: true,
            onSelect: (row, isSelect, rowIndex, e) => {
            //   console.log(row.id);
            //   console.log(isSelect);
            //   console.log(rowIndex);
            //   console.log(e);
              if(isSelect&&!selectedRows.includes(row.id))
                    selectedRows.push(row.id);
              if(!isSelect&&selectedRows.includes(row.id))
                    selectedRows.splice(selectedRows.indexOf(row.id),1);
              
              console.log("the selected stuff are:"+selectedRows);
              this.props.HandleSelect(selectedRows);
            },
            onSelectAll: (isSelect, rows, e) => {
            //   console.log(isSelect);
            //   console.log(rows);
            //   console.log(e);
            if(isSelect)
              console.log("the selected stuff are:"+rows.length);
              if(!isSelect)
                selectedRows=[];
            }
          };
          function priceFormatter(column, colIndex, { sortElement, filterElement }) {
            return (
              <div style={ { display: 'flex', flexDirection: 'column' } }>
                { filterElement }
                { column.text }
                { sortElement }
              </div>
            );
          }
          const defaultSorted = [{
            dataField: 'name', // if dataField is not match to any column you defined, it will be ignored.
            order: 'desc' // desc or asc
          }];
        return (
            <div style={{maxHeight:"70vh", overflow:"overlay", paddingRight:"13px"}}>
                 <div >
                 <BootstrapTable  keyField='id' data={ results } columns={ columns } rowEvents={ rowEvents } 
                 striped hover condensed selectRow={ selectRow } filter={ filterFactory() }
                 defaultSorted={ defaultSorted }/>
                </div>
            </div>
        );
    }
}