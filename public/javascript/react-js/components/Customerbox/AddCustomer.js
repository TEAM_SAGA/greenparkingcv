import React from "react";
import {Col, Row, Form, FormControl} from "react-bootstrap";
import {InputMask} from 'primereact/inputmask';
import {InputText} from 'primereact/inputtext';

import {Button} from 'primereact/button';
export default class AddCustomer extends React.Component{
    constructor(){
        super();
        this.state ={
            id:"",
            name:"",
            phone:"",
            email:"",
        }
        this.handleClick = this.handleClick.bind(this)
    }
    handleChangeEmail(event) {
        this.setState({email: event.target.value})
    }
    handleChangePhone(event) {
        this.setState({phone: event.target.value})
    }
    handleChangeName(event) {
        this.setState({name: event.target.value})
    }
    handleClick (e) {
        e.preventDefault();
        if(!this.state.email) {
            window.alert('Please enter your email');
            return;
        }
        if(!this.state.content) {
            window.alert('Please enter your comment');
            return;
        }

        let support={
            name:this.state.name,
            email:this.state.email,
            phone:this.state.phone,
        }
        let supportJSON = JSON.stringify(support);
        console.log(supportJSON);
        this.setState({email: ""});
        this.setState({name: ""});
        this.setState({phone: ""});
        document.getElementById("create-course-form").reset();
        window.alert('Added customer');
    }
    render(){
        return(
            <div>
                
                <div className="content-section implementation">
                    <div className="ui-g ui-fluid">
                        <div className="ui-g-12 ui-md-6 ui-lg-4">
                            <span>ID: {this.state.id}</span>
                            <InputText  value={this.state.val1} placeholder="313783912" onChange={(e) => this.setState({val1: e.value})}></InputText>
                        </div>

                        <div className="ui-g-12 ui-md-6 ui-lg-4">
                            <span>Full Name: {this.state.name}</span>
                            <InputText value={this.state.val2} placeholder="Eric Freeman" onChange={(e) => this.setState({val2: e.value})}></InputText>
                        </div>

                        <div className="ui-g-12 ui-md-6 ui-lg-4">
                            <span>Phone: {this.state.phone}</span>
                            <InputText  value={this.state.val3} placeholder="050-582-9715" slotchar="mm/dd/yyyy" onChange={(e) => this.setState({val3: e.value})}></InputText>
                        </div>

                        <div className="ui-g-12 ui-md-6 ui-lg-4">
                            <span>Birthday: {this.state.val4}</span>
                            <InputText  value={this.state.val4} placeholder="11/1/1996" onChange={(e) => this.setState({val4: e.value})}></InputText>
                        </div>

                        {/* <div className="ui-g-12 ui-md-6 ui-lg-4">
                            <span>Phone Ext: {this.state.val5}</span>
                            <InputText  value={this.state.val5} placeholder="(999) 999-9999? x99999" onChange={(e) => this.setState({val5: e.value})}></InputText>
                        </div>

                        <div className="ui-g-12 ui-md-6 ui-lg-4">
                            <span>Serial Number: {this.state.val6}</span>
                            <InputText  value={this.state.val6} placeholder="a*-999-a999" onChange={(e) => this.setState({val6: e.value})}></InputText>
                        </div> */}
                    </div>
                </div>
            </div>

        )
    }
}