import React from "react";
import MainDescription from '../components/main_description';
import SideDescription from '../components/side_description';

import { Row, Col, Grid } from 'react-bootstrap'
export default class Home extends React.Component{
    render() {
        const description = "A green parking lot is bordered by Nahal Snir (Nahal Hatzbani) in the northern Galilee, in the northern part of Moshav Beit Hillel.\n" +
            "\n" +
            "                        The site is designed to provide solutions for travelers who wish to stay and enjoy the Hatzbani River, which are rare and reserve in Israel and suitable for overnight stays.\n" +
            "\n" +
            "                        We offer you to enjoy shaded grass areas adjacent to the river water. On site are toilets and hot water showers, drinking water and dishwashing point.\n" +
            "\n" +
            "                        Instead of large Indians tents are built for families to sleep.\n" +
            "                        We will provide you with rustic personal tables and BBQ facilities.\n" +
            "                        The parking lot has shaded parking, night lighting and central power points for charging only cell phones!\n" +
            "                        It is not possible to bring electrical equipment such as plates, etc.\n" +
            "                        Entry is free space.\n" +
            "\n" +
            "                        For pleasant entertainment everyone can not play music or any other noise in the parking lot.\n" +
            "                        The introduction of animals and the burning of fire in places not intended for this purpose are strictly forbidden!"

        return (
            <Grid>
                <Row>
                    <MainDescription customColSize={3} customHeader="something about us" customDescription={description}/>
                    <SideDescription customOffset={3} customColSize={3} customHeader="aside header" customDescription={description}/>
                </Row>
            </Grid>
        );
    }
}