import React from "react";
import {Col, Row,Modal,Carousel} from "react-bootstrap";
import "react-image-gallery/styles/css/image-gallery.css";
import ImageGallery from 'react-image-gallery';
import GalleryRotate from '../components/Gallery_components/GalleryRotate';

export default class Gallery extends React.Component {
    render() {
        return (
            <div className="container">
                <div class="image-gallery-content">
                  <GalleryRotate></GalleryRotate>
               </div>
            </div>
        );
    }
}
