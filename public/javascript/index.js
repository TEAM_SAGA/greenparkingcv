import React from 'react';
import ReactDOM from 'react-dom';
import '/stylesheets/index.css';
import App from '/javascript/react-js/components/App';
import registerServiceWorker from '/javascript/react-js/registerServiceWorker';

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
